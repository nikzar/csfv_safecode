//===-------- Acsl Variable Mapping Pass ------------------------*- C++ -*-===//
//
// The LLVM Compiler Infrastructure - CSFV Annotation Framework
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements a transformation pass that changes the acsl annotations
// mapping the variable names to the registers associated with them by mem2reg
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "safecodevarmap"


#include "safecode/SafecodeVarMap.h"
#include "llvm/Pass.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Function.h"
#include "llvm/Operator.h"
#include "llvm/Instructions.h"
#include "llvm/Instruction.h"
#include "llvm/GlobalVariable.h"
#include "llvm/GlobalValue.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/Metadata.h"
#include "llvm/Constants.h"
#include "llvm/DataLayout.h"
#include "llvm/Target/TargetLibraryInfo.h"

#include "safecode/node.h"
#include "safecode/driver.h" 
#include "llvm/DebugInfo.h"
#include "llvm/Constant.h"
#include "llvm/Support/CFG.h"
//#include "safecode/ranges.h"
#include "llvm/Transforms/Utils/PromoteMemToReg.h"
#include "llvm/Module.h"

#include <stack>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <map>
#include <algorithm>


using namespace llvm;


namespace llvm {

SafecodeVarMap::SafecodeVarMap() : ModulePass(ID){}
SafecodeVarMap::~SafecodeVarMap(){}

//returns the position of op1 in the list of arguments of F
//returns -1 if op1 is not an argument
int SafecodeVarMap::getFunctionArgumentPosition(Value* op1, Function& F) {
	int place=0;
	for(Function::arg_iterator i = F.arg_begin(), e = F.arg_end();i!=e;++i) {
		if(op1==i) {
			return place;
		}
		else
			place++;
	}
	return -1;
}

int SafecodeVarMap::intMallocSize(Value* val, uint64_t sizeBytes, CallInst *call) {
	if(ConstantInt* bitsInt = dyn_cast<ConstantInt>(val)) {
			int64_t bits = bitsInt->getSExtValue();
			int64_t mallocSize = bits/sizeBytes;
			return mallocSize;
	}
	else if(call->getMetadata("acsl_malloc_var_size")){
		if(MDNode *md = dyn_cast<MDNode>(call->getMetadata("acsl_malloc_var_size"))) {
			uint64_t bits1;
			MDString*  annotation1 = dyn_cast<MDString>(md->getOperand(0));
			StringRef ann = annotation1->getString();
			//errs() << ann;
			std::string anns = ann;
			std::stringstream(anns) >> bits1;
			//malloc var size is the math result * size of so we should divide it
			int64_t mallocSize = bits1/sizeBytes;
			return mallocSize;
		}
	} else{
		return -1;
	}

}		

int SafecodeVarMap::getSizeForMalloc(Value *opsb1, Type* ty) {
	Type* typ = ty->getPointerElementType();
	uint64_t sizeBits = typ->getPrimitiveSizeInBits();
	uint64_t sizeBytes=sizeBits/8;
	if(CallInst *call = dyn_cast<CallInst>(opsb1)){
		Function* callee = call->getCalledFunction();
		StringRef cName = callee->getName();
		if(cName.compare("malloc")==0) {
			//val: i64 8000
			Value* val = call->getArgOperand(0);
			//val->print(errs());
			return intMallocSize(val, sizeBytes, call);
		} 
	} 
	return -1;
}

int SafecodeVarMap::getMinimumMallocSizeFromFunction(Function &F){
	//get the minimum of size for the value returned
	int minimum = -2;

	//loop over the return instructions of the function
	for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
		//errs()<<"\t"<<*I<<"\n";
		if(ReturnInst * retInst = dyn_cast<ReturnInst>((&*I))){
			//errs()<<"Searching malloc size for: "<< *retInst <<"in function: "<<F.getName()<<"\n";
			//errs()<<"\t retval:"<<*(retInst->getReturnValue())<<"\n";
			//get the bitcast and call getSizeForMalloc there
			if(BitCastInst *bitcast = dyn_cast<BitCastInst>(retInst->getReturnValue())){
				Value* opsb1 = bitcast->getOperand(0);
				Type* ty = bitcast->getDestTy();
				
				//get the minimum between the temporary minmum and this one
				int tempsize = getSizeForMalloc(opsb1, ty);
				//first iteration
				if(minimum==-2){
					minimum = tempsize;
				} 
				else {
					minimum = std::min(minimum,tempsize);
				}
				//errs() << "\tsize for return:"<<tempsize<<"\n";
				
			}
		}
	}
	return minimum;			
}

void SafecodeVarMap::handleBitCastInst(GetElementPtrInst *GEP, BitCastInst *bitcast){
	//opsb1: %var0 = call noalias i8* @malloc(i64 8000)
	Value* operand = bitcast->getOperand(0);;
	Type* bitcastType = bitcast->getDestTy();
	int size = getSizeForMalloc(operand, bitcastType);
	std::stringstream strstream;
	strstream << size;
	std::string mallocSizeAnnotation = strstream.str();
	LLVMContext& C = GEP->getContext();
	MDNode* V = MDNode::get(C, MDString::get(C, mallocSizeAnnotation));
	GEP->setMetadata("acsl_malloc", V);
}

void SafecodeVarMap::handleAllocationByFunctionCall(GetElementPtrInst *GEP, CallInst *CI){
	//call run on function to be sure that the acsl_malloc_var_size is there
	if(Function* callee = CI->getCalledFunction()){
		StringRef cName = callee->getName();
		if(cName.compare("malloc")!=0) {
			if(visitedFunctions.count(cName.str())==0){
				runOnFunction(*callee);
			}
			int mallocsize = getMinimumMallocSizeFromFunction(*callee);
			
			if(mallocsize>0){
				//attach the metadata to the GEP
				std::stringstream strstream;
				strstream << mallocsize;
				std::string mallocSizeAnnotation = strstream.str();
				LLVMContext& C = GEP->getContext();
				MDNode* V = MDNode::get(C, MDString::get(C, mallocSizeAnnotation));
				GEP->setMetadata("acsl_malloc", V);
			}
			
		}
	}
}

void SafecodeVarMap::handleFunctionArgument(GetElementPtrInst *GEP, Value *startMemoryLocation){
	Function &F = *GEP->getParent()->getParent();
	
	//get the place of op1 in the list of args, say it is the nth argument
	//get the list of calls to F.
	int place = getFunctionArgumentPosition(startMemoryLocation, F);
	//initiliaze minimum
	int minimum = -2;
	
	for(Value::use_iterator i = F.use_begin(), e= F.use_end(); i!=e;++i) {
		if(Instruction*Inst = dyn_cast<Instruction>(*i)) {
			std::string functionName = Inst->getParent()->getParent()->getName().str();
			//errs() << "F is used in instruction: "<< *Inst<<"in function:"<< functionName<<"\n";
			if(visitedFunctions.count(functionName)==0){
				runOnFunction(*(Inst->getParent()->getParent()));
			}
			//ArgumentListType arglist = F.getArgumentList();
			if(CallInst* call = dyn_cast<CallInst>(Inst)) {
				Value* val = call->getArgOperand(place);
				if(BitCastInst *bitcast1 = dyn_cast<BitCastInst>(val)) {
					//opsb1: %var0 = call noalias i8* @malloc(i64 8000)
					Value* opsb2;
					Type* ty1;
					opsb2= bitcast1->getOperand(0);
					ty1 = bitcast1->getDestTy();
					//get the size for the place where the function is call
					//get the minimum between the temporary minmum and this one
					int tempsize = getSizeForMalloc(opsb2, ty1);
					//first iteration
//													if(minimum==-2){
//														minimum = tempsize;
//													} 
//													else {
//														minimum = std::min(minimum,tempsize);
//													}
					
					//WRONG!!! JUST FOR NOW!!!
					if(tempsize!=-1){
						if(minimum==-2){
							minimum = tempsize;
						} 
						else {
							minimum = std::min(minimum,tempsize);
						}
					}
					//END WRONG
					
				}
				//if it is a function call that is not a malloc
				else if (CallInst *CI = dyn_cast<CallInst>(val)) {
					//call run on function to be sure that the acsl_malloc_var_size is there
					if(Function* callee = CI->getCalledFunction()){
						StringRef cName = callee->getName();
						if(cName.compare("malloc")!=0) {
							//if we have not already visited that function
							if(visitedFunctions.count(cName.str())==0){
								runOnFunction(*callee);
							}
							int tempsize = getMinimumMallocSizeFromFunction(*callee);
							//WRONG!!! JUST FOR NOW!!!
							if(tempsize!=-1){
								if(minimum==-2){
									minimum = tempsize;
								} 
								else {
									minimum = std::min(minimum,tempsize);
								}
							}
							//END WRONG
						}
					}
				}
				//else if the argument is a GEP with an acsl_malloc metadata replicate it here
				if(GetElementPtrInst * GEP = dyn_cast<GetElementPtrInst>(val)){
					if (MDNode *N = GEP->getMetadata("acsl_malloc")){
						int tempsize =-1;
						if(MDString*  annotation = dyn_cast<MDString>(N->getOperand(0))){
							std::stringstream(annotation->getString()) >> tempsize;
						}
						//WRONG!!! JUST FOR NOW!!!
						if(tempsize!=-1){
							if(minimum==-2){
								minimum = tempsize;
							} 
							else {
								minimum = std::min(minimum,tempsize);
							}
						}
						//END WRONG
					}
				}
				
			}
		}
	}
	//get the minimum, if it is <0 do not attach anything, else attach the min
	if(minimum>0){
		//attach the metadata to the GEP
		std::stringstream strstream;
		strstream << minimum;
		std::string mallocSizeAnnotation = strstream.str();
		LLVMContext& C = GEP->getContext();
		MDNode* V = MDNode::get(C, MDString::get(C, mallocSizeAnnotation));
		GEP->setMetadata("acsl_malloc", V);
	}
}

void SafecodeVarMap::handleGlobalArray(GetElementPtrInst *GEP, GlobalVariable* gv){
	Type* typ = gv->getType();//[10000 x i32]*
	//errs() <<"Type: "<<*typ<<"\n";
	Type* pointedType=typ->getPointerElementType();//[10000 x i32]
	//errs() <<"Pointed Type: "<< *pointedType <<"\n";
	if(isa<ArrayType>(pointedType)){ //OK we are accessing an array (which is global)
		int numElements = pointedType->getArrayNumElements();
		//errs() << "array num elements: " << numElements << "\n";
		std::stringstream strstream;
		strstream << numElements;
		std::string mallocSizeAnnotation = strstream.str();
		LLVMContext& C = GEP->getContext();
		MDNode* V = MDNode::get(C, MDString::get(C, mallocSizeAnnotation));
		GEP->setMetadata("acsl_malloc", V);
	}
}

void SafecodeVarMap::attachMallocMetadata(GetElementPtrInst *GEP){
	Value *startMemoryLocation = GEP->getOperand(0);
	//replicate acsl_malloc metadata if starting from a GEP
	if(GetElementPtrInst * GEPStart = dyn_cast<GetElementPtrInst>(startMemoryLocation)){
		if (MDNode *N = GEPStart->getMetadata("acsl_malloc")){
			GEP->setMetadata("acsl_malloc",N);
		}
	} 
	//if is a malloc of known size
	else if(BitCastInst *bitcast = dyn_cast<BitCastInst>(startMemoryLocation)) {
		handleBitCastInst(GEP,bitcast);
	}
	//if it is not a malloc analyze function call (calle)
	else if (CallInst *CI = dyn_cast<CallInst>(startMemoryLocation)) {
		handleAllocationByFunctionCall(GEP, CI);
	}
	//if it is an argument of the function
	else if((getFunctionArgumentPosition(startMemoryLocation, *(GEP->getParent()->getParent())))!=-1) {
		handleFunctionArgument(GEP, startMemoryLocation);
	}
	else if(GlobalVariable* GV = dyn_cast<GlobalVariable>(startMemoryLocation)) {
		handleGlobalArray(GEP, GV);
	}
						
}

bool SafecodeVarMap::runOnModule(Module &M){	
	bool changed = false;
	for(Module::iterator FI = M.begin(), e = M.end(); FI != e; ++FI){
		StringRef fName = FI->getName();
		if(!fName.startswith("llvm.")) {
			bool tempchanged = false;
			//errs()<<"Module pass running on: "<<FI->getName()<<"\n";
			if(visitedFunctions.count(fName.str())==0){
				tempchanged = runOnFunction(*FI);
			}
			if(tempchanged){
				changed = true;
			}
		}
	}
	return changed;
}

void SafecodeVarMap::assignFreshNamesToUnamedInstructions(Function & F){
	int nameCount=0;
	for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
		if((!I->hasName())&&(!(I->getType()->isVoidTy()))) {
			std::stringstream ss;
			ss << "var"<<nameCount;
			std::string s = ss.str();
			I->setName(s);
			nameCount++;
		}
	}
}

void SafecodeVarMap::collectVariableMappingInformations(Function &F, StringMap< std::vector<VarMappingInfo> > &mappingMap, StringMap< std::string > &reverseNameSCMap, StringMap< Value * > &reverseContextMap,StringMap< int > &reverseCodeLineMap,StringMap< std::vector<std::string> > &constantsMap){
	for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
		if(CallInst *CI = dyn_cast<CallInst>(&*I)){
			if( CI->getCalledFunction()){
				//void %llvm.dbg.value(metadata, i64, metadata)
				//This intrinsic provides information when a user source variable is set to a new value.
				//The first argument is the new value (wrapped as metadata).
				//The second argument is the offset in the user source variable where the new value is written.
				//The third argument is metadata containing a description of the user source variable.
				if( CI->getCalledFunction()->getName()==StringRef("llvm.dbg.value") ){
					//get the line of code
					int codeLine = 0;
					if (MDNode *N = CI->getMetadata("dbg")) {
						DILocation Loc(N);
						unsigned Line = Loc.getLineNumber();
						codeLine = Line;
					}
					// could be the value information about an IR register, or a string with the annotation
					MDNode * MD1 = cast<MDNode>(CI->getOperand(0));
					//if it is an annotation
					if(ConcreteOperator<Operator,Instruction::GetElementPtr> *CO=
					   dyn_cast<ConcreteOperator<Operator,Instruction::GetElementPtr> >(MD1->getOperand(0))) {
						if(CO->getNumOperands()>0){
							if(GlobalVariable *GV = dyn_cast<GlobalVariable>(CO->getOperand(0))){
								if(GV->hasInitializer()){
									if (ConstantDataArray *CDA =
										dyn_cast<ConstantDataArray>(GV->getInitializer())){
										if(CDA->isString()){
											//do nothing, it is just an annotation and it is too early to parse it
										}
									}
								}
								
							}
						}
						
					}
					//if it is a value information
					else {
						MDNode * MD2 = cast<MDNode>(CI->getOperand(2));
						StringRef nameSC = MD2->getOperand(2)->getName();
						Value * context = MD2->getOperand(1);
						
						std::string nameIR;
						//if it is a constant. For assignments of the type i=0; j=5; i=7; i=5, where rhs is a constant
						   // we build constantsMap as a map <<0,i>,<5,<j,i>>, <7,i>>
						   //this is needed for phi nodes.
						//TODO: Support also non Int constants values
						if(ConstantInt * MD11 = dyn_cast<ConstantInt>(MD1->getOperand(0))){
							if (MD11->getBitWidth() <= 32) {
								int constIntValue = MD11->getSExtValue();
								std::ostringstream o;
								o << constIntValue;
								nameIR = o.str(); //we are substituting the name in the IR with the value
														//because we will substitute it in the annotation.
														//so instead of "i" we will have "0". and instead of
														//"i==0", we will have "0==0"
								//std::string nameIRstr = o.str();
								constantsMap[nameIR].push_back(nameSC);
							}
						}
						//if in the debug information the name refers not to a constant but to an 
							//variable in the IR (which incidentally is the same name of the instruction)
							//we get the name of that IR variable in nameIR, by looking at the first operand in the dbg metadata.
						else {
							nameIR = MD1->getOperand(0)->getName();
						}
						//just found a new nameIR variable, so create a VarMappingInfo
						VarMappingInfo newInfo = VarMappingInfo(context, CI->getParent(),nameIR, codeLine);
						
						mappingMap[nameSC].push_back(newInfo);
						
						reverseNameSCMap[nameIR] = nameSC;
						reverseContextMap[nameIR] = context;
						reverseCodeLineMap[nameIR] = codeLine;
						
					}
				}
			}
		}
	}
	
}

void SafecodeVarMap::collectFirstLineBasciBlock(Function &F, std::map< BasicBlock*, int > &firstLineBasciBlockMap){
	for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb){
		// Print out the name of the basic block if it has one, and then the
		// number of instructions that it contains
		int firstLine = 0;
		for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i){
			if(MDNode * N = (&*i)->getMetadata("dbg")){
				DILocation Loc(N);
				firstLine = Loc.getLineNumber();
				break;
			}
		}
		firstLineBasciBlockMap[bb] = firstLine;
		
	}
}

void SafecodeVarMap::insertPhiNodeInformations(Function &F, StringMap< std::vector<VarMappingInfo> > &mappingMap, StringMap< std::string > &reverseNameSCMap, StringMap< Value * > &reverseContextMap,StringMap< int > &reverseCodeLineMap,StringMap< std::vector<std::string> > &constantsMap,std::map< BasicBlock*, int > &firstLineBasciBlockMap){
	int maxIter = 5;
	int numIter = 0;
	bool nestedPHI;
	do {
		nestedPHI = false;
		for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
			//get information if it is a variable assigned in a PHI Node
			if (PHINode *PHI = dyn_cast<PHINode>(&*I)){
				
				//get the next line of code
				int codeLine = 0;
				BasicBlock * blk = PHI->getParent();
				BasicBlock::iterator it(PHI);
				++it; // After this line, it refers to the instruction after *inst
				Instruction * NextInstr = &*it;
				MDNode *N;
				//searching the nearest debug info in the same block
				//while the next instr has not dbg info and it is in the same block 
				//switch to the nex instruction ater this one
				while (true){
					if(blk==NextInstr->getParent()){
						if((N = NextInstr->getMetadata("dbg"))){
							break;
						} else {
							++it;
							NextInstr = &*it;
						}
						
					} else {
						break;
					}
				}
				
				DILocation Loc(N);
				unsigned Line = Loc.getLineNumber();
				codeLine = Line;
				
				
				StringRef nameIR = PHI->getName();
				
				//get the name of the SC variable associated to one on the phi fun
				unsigned int i = 0;
				StringRef namePhiOp = "";
				bool allConstants = true;
				do{
					if(dyn_cast<ConstantInt>(PHI->getOperand(i))){
						//do nothing
					}
					//if it is a variable name
					else {
						allConstants = false;
						namePhiOp = PHI->getOperand(i)->getName();
					}
					i++;
				} while (i<PHI->getNumOperands() && namePhiOp=="");
				
				StringRef nameSC = reverseNameSCMap[namePhiOp];
				
				// POSSIBLE BUG: we can't do nothing better that take the most probable name
				// if all the phi function arguments are constant
				if (allConstants){
					unsigned i = 0;
					StringMap<int> countConst;
					// for every constant find how many times a variable is mapped to that constant
					do{
						std::string constName = "";
						if(ConstantInt * MD11 = dyn_cast<ConstantInt>(PHI->getOperand(i))){
							if (MD11->getBitWidth() <= 32) {
								int constIntValue = MD11->getSExtValue();
								std::ostringstream o;
								o << constIntValue;
								constName = o.str();
							}
						}

						for (std::vector<std::string>::iterator j = constantsMap[constName].begin(); j!=constantsMap[constName].end(); j++) {
							std::string temp =(*j);
							countConst[temp]=countConst[temp]+1;
						}
						i++;
					} while (i<PHI->getNumOperands());
					

					//select the first name that match all the constant
					for(StringMapIterator<int> z = countConst.begin(); z!=countConst.end();++z){
						unsigned count = z->getValue();
						if(count==i){
							nameSC = z->getKey();
							break;
						}
					}
					
					//get the context from the next inst
					Value * context;
					BasicBlock * blk = PHI->getParent();
					BasicBlock::iterator it(PHI);
					++it; // After this line, it refers to the instruction after *inst
					Instruction * NextInstr = &*it;
					MDNode *N;
					while ( (!(N = NextInstr->getMetadata("dbg"))) && (blk==NextInstr->getParent())){
						++it;
						NextInstr = &*it;
					}
					if(N){
						context = N->getOperand(2);
					}
					else{
						context = NULL;
					}
					
					//add the phi var at the next instruction line+1
					mappingMap[nameSC].push_back(VarMappingInfo(context, PHI->getParent(), nameIR, codeLine));
					reverseNameSCMap[nameIR] = nameSC;
					reverseContextMap[nameIR] = context;
					reverseCodeLineMap[nameIR] = codeLine+1;
					
					//loop over the instruction of the block
					BasicBlock * BB = PHI->getParent();
					bool phiRedifined = false;
					for (BasicBlock::iterator i = BB->begin(), e = BB->end(); i != e; ++i){
						//if there is a llvm.debug.value about that variable stop
						if(CallInst *CI = dyn_cast<CallInst>(&*i)){
							if( CI->getCalledFunction()){
								if( CI->getCalledFunction()->getName()==StringRef("llvm.dbg.value") ){
									MDNode * MD2 = cast<MDNode>(CI->getOperand(2));
									StringRef nameVAr = MD2->getOperand(2)->getName();
									if (nameVAr.equals(nameSC)) {
										phiRedifined = true;
										break;
									}
								}
							}
							
						}
						
					}
					
					//if it reaches the end get all the block successors
					if(!phiRedifined){
						for (succ_iterator PI = succ_begin(BB), E = succ_end(BB); PI != E; ++PI) {
							//for each of them add the definition of the phi variable at
							//the first line
							StringRef blockName = PI->getName();
							codeLine = firstLineBasciBlockMap[*PI];
							mappingMap[nameSC].push_back(VarMappingInfo(context, PHI->getParent(), nameIR, codeLine));
						}
					}
					
					DEBUG(
						  printMappingMap(&mappingMap);
						 );

				
				}
				// didn't succeed to resolve the phi nameSC
				else if (nameSC=="") {
					nestedPHI=true;
					numIter++;
				}
				// normally add the phi mapping
				else {
					Value * context = reverseContextMap[namePhiOp];
					
					
					//add the phi var at the next instruction line+1
					mappingMap[nameSC].push_back(VarMappingInfo(context, PHI->getParent(), nameIR, codeLine));
					reverseNameSCMap[nameIR]=nameSC;
					reverseContextMap[nameIR] = context;
					reverseCodeLineMap[nameIR] = codeLine+1;
					
					//loop over the instruction of the block
					BasicBlock * BB = PHI->getParent();
					bool phiRedifined = false;
					for (BasicBlock::iterator i = BB->begin(), e = BB->end(); i != e; ++i){
						//if there is a llvm.debug.value about that variable stop
						if(CallInst *CI = dyn_cast<CallInst>(&*i)){
							if(CI->getCalledFunction()){
								if( CI->getCalledFunction()->getName()==StringRef("llvm.dbg.value") ){
									MDNode * MD2 = cast<MDNode>(CI->getOperand(2));
									StringRef nameVAr = MD2->getOperand(2)->getName();
									if (nameVAr.equals(nameSC)) {
										phiRedifined = true;
										break;
									}
								}
								
							}
						}
						
					}
						
					//if it reaches the end get all the block successors
					if(!phiRedifined){
						for (succ_iterator PI = succ_begin(BB), E = succ_end(BB); PI != E; ++PI) {
							//for each of them add the definition of the phi variable at
							//the first line
							StringRef blockName = PI->getName();
							codeLine = firstLineBasciBlockMap[*PI];
							mappingMap[nameSC].push_back(VarMappingInfo(context, PHI->getParent(), nameIR, codeLine));
						}
					}
					
					DEBUG(
						  printMappingMap(&mappingMap);
						  );
				}
			}

		}
	} while (nestedPHI && (numIter<maxIter));
	
}

bool SafecodeVarMap::runOnFunction(Function &F){
	std::string fName = F.getName().str();
	//errs() << "SafecodeVarMap::runOnFunction: " << fName << "\n";
	
	//keep track of the analyzed functions
	visitedFunctions.insert(fName);
	// loop (1) FILL A DATA STRUCTURE WITH THE INFORMATIONS GATHERED BY THE DUBUG INFO
	//VarMappingInfo is a data structure that holds several pieces of info about
	//a variable. That is: 1) context (a.k.a scope in the source file of the original 
	// source variable to which this IR variable refers,
	//each scope is an entity in llvm), 2) basic block in the IR where the IR variable 
	//is, 3) nameIR, the name of this variable (i.e. register) in the IR
	//4) line in the source code, this IR var refers to.
	StringMap< std::vector<VarMappingInfo> > mappingMap;
	//reverseNameSCMap: <irName, sourceCodeName>. This is one of the results of the
	//computations.
	StringMap< std::string > reverseNameSCMap;
	//reverseContextMap: <irName, context>. Given an IR name tells the context in the source code
	StringMap< Value * > reverseContextMap;
	//same as above for line number (codeline)
	StringMap< int > reverseCodeLineMap;
	//this is for understanding which variable a phi-node refers to when the
	//values of the alternatives inside the phinode are all constants (we do not have
	//debug info for phi nodes). So we go over the constant values and check
	//which variable they may belong to and store the results in this map:
	//<constant, nameIR>.
	StringMap< std::vector<std::string> > constantsMap;
	//SafecodeInfo is a: <varname, annotation>. For each variable in an
	//instruction, gives the acsl annotation.
	//Each Instruction may have more than one variables.
	std::map<Instruction *, std::map<std::string, ranges::Range> > safecodeMap;
	
	
	//the loop below goes over all instructions and renames
	//those that do not have a name, which appear in the IR
	//with %n, where n is a number. This is present in Rigel's
	//setup (safecode, LLVM 3.2) but not on Niko's setup (LLVM 3.3, no safecode)
	//This apparently messes up Niko's code on rigel's setup, and it misses
	//some assertions. The loop below to try fix this.
	assignFreshNamesToUnamedInstructions(F);
	
	//This loop populates the VarMappingInfo structure
	collectVariableMappingInformations(F, mappingMap, reverseNameSCMap, reverseContextMap,reverseCodeLineMap,constantsMap);
	
	// loop (2) over the basicbolcks and get the first line of code. deals with phi-fnctions
	std::map< BasicBlock*, int > firstLineBasciBlockMap;
	collectFirstLineBasciBlock(F,firstLineBasciBlockMap);

	// loop (3) over phi functions
	insertPhiNodeInformations(F, mappingMap, reverseNameSCMap, reverseContextMap,reverseCodeLineMap,constantsMap, firstLineBasciBlockMap);

	// loop (4) over the annotations and change them accordingly to the right mapping
	for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
		if(CallInst *CI = dyn_cast<CallInst>(&*I)){
			if( CI->getCalledFunction()){
				if( CI->getCalledFunction()->getName()==StringRef("llvm.dbg.value") ){
				//check if it contains a string with the annotation
				MDNode * MD1 = cast<MDNode>(CI->getOperand(0));
				//if it is an annotation
				if(ConcreteOperator<Operator,Instruction::GetElementPtr> *PI =
				   dyn_cast<ConcreteOperator<Operator,Instruction::GetElementPtr> >(MD1->getOperand(0))) {
					if(GlobalVariable *GI = dyn_cast<GlobalVariable>(PI->getOperand(0))){
						if(GI->hasInitializer()){
							if (ConstantDataArray *GV =
								dyn_cast<ConstantDataArray>(GI->getInitializer())){
								if(GV->isString()){
									
									//drop front to remove the @ and the last (strange symbol)
									StringRef annotation = GV->getAsString().drop_back(1).drop_front(1);
									
									
									//get the line of code and the context (scope of the annotation)
									int codeLine = 0;
									MDNode * annotationContext;
									if (MDNode *N = CI->getMetadata("dbg")) {
										DILocation Loc(N);
										unsigned Line = Loc.getLineNumber();
										codeLine = Line;
										annotationContext = cast<MDNode>(N->getOperand(2));
									}
									DEBUG(
										  //errs()<<annotation<<" -> "<< annotationContext <<" @ "<<codeLine<<"\n";
									);
									DEBUG(
										  //errs()<<"Annotation context -> ";
										  //annotationContext->print(errs());
										  //errs()<<"\n";
									);
									//Parse the string
									ACSLStatements root;
									example::Driver driver(root);
									//errs()<<"Parsing: "<<annotation<<"\n";
									bool result = driver.parse_string(annotation, "input");
									if (result){
										std::string lastNameIR = "";
										//For every variable search in the data structure the correct mapping
										std::set<std::string> varList = driver.root.getTreeVariables();
										for (std::set<std::string>::iterator j = varList.begin(); j != varList.end(); ++j){
											
											BasicBlock * basicBlock = CI->getParent();
											std::string nameSC = *j;
											
											
											std::string correctNameIR = findNameIR(&mappingMap,annotationContext,basicBlock, nameSC, codeLine);
											
											//Substitute the variable name with the correct one
											driver.root.changeTreeVariableName(correctNameIR, nameSC);
											
											//save the var about the annotation
											lastNameIR=correctNameIR;
										}
										DEBUG(
											  //errs()<<driver.root.getTreePrintString()<<"\n";
										);
										//Store the annotations in safecode map
										for (std::vector<ACSLStatement*>::iterator istmt = driver.root.statements.begin(); istmt != driver.root.statements.end(); ++istmt)
										{
											ACSLStatements root2;
											example::Driver driver2(root2);
											if(ACSLAssertStatement * stmt = dyn_cast<ACSLAssertStatement>((*istmt))){
																									   
												auto sp = getRangePair(&(stmt->exp));

												DEBUG(
													//dbgs()<<"Inserted info @"<<(&*I)->getName()<<": "<< sp.first <<" -> "<<sp.second<<" in safecodeMap\n";
												);
													
												safecodeMap[&*I][sp.first]=sp.second;
											}
											
										}
																					
									}
									
								}
							}
						}
					}
				}
			}
			}
		}
	}

	// loop (5) Safecode: Iterate over the instruction in the function
	for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I){
		//if it has an annotation get the varname
		if(safecodeMap.count(&*I)){
			//safecodeMap will have the pair with the ranges
			//iterate over all the following instructions in the same BB
			BasicBlock::iterator it((&*I));
			while (Instruction * NextInst = it->getNextNode()) {
				//errs()<<*NextInst<<"\n";
				//if there is an annotation about the same variable stop???
				if (safecodeMap.count(NextInst)) {
					break;
				}                        
				//if it is an algebric operation keep track
				else if(BinaryOperator * BI = dyn_cast<BinaryOperator>(NextInst)){
					bool rangeFound = true;
					Instruction::BinaryOps opcode = BI->getOpcode();
					ranges::Range range1;
					if(ConstantInt * CI = dyn_cast<ConstantInt>(BI->getOperand(0))){
						int num = CI->getSExtValue();
						range1 = ranges::Range(num,num);
					} else if (safecodeMap[(&*I)].count(BI->getOperand(0)->getName())){
						range1 = safecodeMap[(&*I)][BI->getOperand(0)->getName()];
					} else {
						rangeFound = false;
					}
					ranges::Range range2;
					if(ConstantInt * CI = dyn_cast<ConstantInt>(BI->getOperand(1))){
						int num = CI->getSExtValue();
						range2 = ranges::Range(num,num);
					} else if (safecodeMap[(&*I)].count(BI->getOperand(1)->getName())){
						range2 = safecodeMap[(&*I)][BI->getOperand(1)->getName()];
					} else {
						rangeFound = false;
					}
					if(rangeFound){
						ranges::Range result;
						bool supportedInst = true;
						switch (opcode) {
							//if it is a add compute range and update the map with %add_name -> range*
							case (Instruction::Add):
								result = range1 + range2;
								break;                     
								//if it is a sub compute range and update the map with %sub_name -> range*
							case (Instruction::Sub):
								result = range1 - range2;
								break;
								//if it is a mul compute range and update the map with %mul_name -> range*
							case (Instruction::Mul):
								result = range1 * range2;
								break;
							default:
								supportedInst = false;
						}
						//errs()<<"adding "<<BI->getName()<<"->"<<result<<"\n";
						if (supportedInst){
							safecodeMap[(&*I)][BI->getName()]=result; 
						}
					}
				}
				//if the variable is reassigned i64->i32 keep track
				else if(SExtInst * SI = dyn_cast<SExtInst>(NextInst)){
					std::string sextName = SI->getOperand(0)->getName();
					if(safecodeMap[(&*I)].count(sextName)){
						safecodeMap[(&*I)][SI->getName()]=safecodeMap[(&*I)][sextName];
					}
				}
				//if the variable is reassigned i64->i32 keep track
				else if(ZExtInst * ZI = dyn_cast<ZExtInst>(NextInst)){
					std::string zextName = ZI->getOperand(0)->getName();
					if(safecodeMap[(&*I)].count(zextName)){
						safecodeMap[(&*I)][ZI->getName()]=safecodeMap[(&*I)][zextName];
					}
				}
				//if it is a malloc if we are abele to compute size, attach acsl_malloc metadata
				else if(CallInst * call = dyn_cast<CallInst>(NextInst)){
					if(Function* callee = call->getCalledFunction()){
						StringRef cName = callee->getName();
						if(cName.compare("malloc")==0) {
						//val: i64 %var2
						Value* val = call->getArgOperand(0);
							std::string mallocOpName = val->getName();
							if(safecodeMap[(&*I)].count(mallocOpName)){
								ranges::Range r = safecodeMap[(&*I)][mallocOpName];
								std::stringstream strstream;
								strstream << r.getBottom();
								std::string mallocSizeAnnotation = strstream.str();
								LLVMContext& C = NextInst->getContext();
								MDNode* V = MDNode::get(C, MDString::get(C, mallocSizeAnnotation));
								call->setMetadata("acsl_malloc_var_size", V);
							}
						}
					}
				}
				//if the GEP is using the var attach the metadata "acsl_safecode"
				else if(GetElementPtrInst * GEP = dyn_cast<GetElementPtrInst>(NextInst)){
					//attach acsl_safecode metadata and keep track
					if(GEP->getNumOperands()>2){
						//errs()<<*GEP<<"\n";
						std::string gepName = GEP->getOperand(2)->getName();
						if(safecodeMap[(&*I)].count(gepName)){
							ranges::Range r = safecodeMap[(&*I)][gepName];
							LLVMContext& C = NextInst->getContext();
							MDNode* V = MDNode::get(C, MDString::get(C, r.getAnnotation(gepName)));
							GEP->setMetadata("acsl_safecode", V);
							safecodeMap[(&*I)][GEP->getName()] = r;
						} 
					}
					if(GEP->getNumOperands()==2){
						std::string gepStartName = GEP->getOperand(0)->getName();
						std::string gepOffsetName = GEP->getOperand(1)->getName();
						if(safecodeMap[(&*I)].count(gepOffsetName)){
							ranges::Range result = safecodeMap[(&*I)][gepOffsetName];
							LLVMContext& C = NextInst->getContext();
							//if there are info about the start name compute the range from there
							if(safecodeMap[(&*I)].count(gepStartName)){
								result = result + safecodeMap[(&*I)][gepStartName];
							}
							MDNode* V = MDNode::get(C, MDString::get(C, result.getAnnotation(gepOffsetName)));
							GEP->setMetadata("acsl_safecode", V);
							safecodeMap[(&*I)][GEP->getName()] = result;
						} 
					}
						
					attachMallocMetadata(GEP);
					
				}
				//if the STORE is using the var attach the metadata "acsl_safecode"
				else if(StoreInst * STI = dyn_cast<StoreInst>(NextInst)){
					std::string gepName = STI->getOperand(1)->getName();
					if(safecodeMap[(&*I)].count(gepName)){
						ranges::Range r = safecodeMap[(&*I)][gepName];
						LLVMContext& C = NextInst->getContext();
						MDNode* V = MDNode::get(C, MDString::get(C, r.getAnnotation(gepName)));
						STI->setMetadata("acsl_safecode", V);
					}
					
				}
				//if the LOAD is using the var attach the metadata "acsl_safecode"
				else if(LoadInst * LDI = dyn_cast<LoadInst>(NextInst)){
					std::string gepName = LDI->getOperand(0)->getName();
					if(safecodeMap[(&*I)].count(gepName)){
						ranges::Range r = safecodeMap[(&*I)][gepName];
						LLVMContext& C = NextInst->getContext();
						MDNode* V = MDNode::get(C, MDString::get(C, r.getAnnotation(gepName)));
						LDI->setMetadata("acsl_safecode", V);
						//replicate the range about the variable in the load
						//TODO: do it only if it is a LOAD about global variable
						safecodeMap[(&*I)][LDI->getName().str()]=r;
					}
				}
				++it;
			}
		}
	}
   return true;
}

//input: 
//findNameIR, gets in input mappingMap, the context where the annotation is in the source code
//the basic block in the IR where the annotation is and the line number, and the name of the variable
//in the source code, which we got by parsing the annotation.
std::string SafecodeVarMap::findNameIR(StringMap< std::vector<VarMappingInfo> > * mappingMap, MDNode *  annotationContext,BasicBlock * basicBlock, std::string  nameSC, int codeLine){
	MDNode * context = annotationContext;
	
	std::string nameIR = nameSC;
	
	//lookup the data structure for the correct mapping
	bool hasSuperContext= true;
	// if there is more than one parent there should be a phi function...
	bool hasParent = true;
	bool foundName = false;
	
	std::map<BasicBlock*, bool> visitedBasicBlocks;
	std::stack<BasicBlock *> blockStack;
	blockStack.push(basicBlock); //this is the current basic block, where the annotation is
	//loop over the chain of context
	 //So, we start at the current scope of the annotation
	 //and look for all the possible mapping candidates starting from the current block
	 //A candidate is a variable whose value is being set (that is a definition) and whose
	 //corresponding source code name is the same as the name of the variable
	 //in the annotation. If we find one in the current block we are done.
	 //Otherwise we look at all the predecessor blocks having the same scope. If we do not find
	 // a definition there, we start with the parent scope (also called context below) and repeat the same thing.
	 //Until we reach the global scope.
	while (hasSuperContext && hasParent && !foundName) { //hasSuperContext refers to the parent scope
		if (blockStack.empty()) {							//hasParent refers to the block.
			//	errs()<<"block stack is empty\n";
			if(context && (context->getNumOperands()>1)){ //context is the same as scope.
				
				if (MDNode * MDP = dyn_cast<MDNode>(context->getOperand(1)) ){
					
				 //   errs()<<"Searching in top context ->"<<MDP<<"\n";
					
					context = MDP;
					blockStack.push(basicBlock);
					visitedBasicBlocks.clear();
				 }
					else
					 hasSuperContext=false;
			}
			else{
				hasSuperContext=false;
				
			}
		}
		else {
			//take the top element on the stack of blocks to visit
			BasicBlock * currentBlock = blockStack.top();
			
			   //   errs()<<"\tSearching in predecessor block ->"<< currentBlock <<"\n";
			
			blockStack.pop();
			visitedBasicBlocks[currentBlock] = true;
			
			//find all the mappings in the same context and basicBlock
			std::vector<VarMappingInfo> sameContextAndBasicBlockVector;
			for (std::vector<VarMappingInfo>::iterator z = (*mappingMap)[nameSC].begin(); z!=(*mappingMap)[nameSC].end(); ++z) {
				//check if there is a variable map in the same context
				VarMappingInfo info = (*z);
				if(context == info.context && currentBlock == info.basicBlock && info.codeLine<=codeLine){
					sameContextAndBasicBlockVector.push_back(info);
				}
			}
			if (sameContextAndBasicBlockVector.size()!=0) {
				//search for the nearest mapping in the vector
				VarMappingInfo closest = sameContextAndBasicBlockVector.front();
				for (std::vector<VarMappingInfo>::iterator i = sameContextAndBasicBlockVector.begin(); i!=sameContextAndBasicBlockVector.end(); ++i) {
					VarMappingInfo info = *i;
					if(info.codeLine<=codeLine){
						if(info.codeLine>closest.codeLine){
							closest=info;
						}
					}
				}
				nameIR = closest.nameIR;
				foundName=true;
				break;
				
				
			} else {
				//popolate the stack with all the predecessor that are not already visited
				for (pred_iterator PI = pred_begin(currentBlock), E = pred_end(currentBlock); PI != E; ++PI) {
					if(visitedBasicBlocks.count((*PI))==0){
						blockStack.push((*PI));
					}
				}
				
			}
		}
		
	}
	return nameIR;
	
}


void SafecodeVarMap::printMappingMap(StringMap< std::vector<VarMappingInfo> > * mappingMap){
	//errs()<<"===-------------------------------------------------------------------------===\n";
	//errs() << "               ... Mapping Informations ...\n";
	//errs()<<"===-------------------------------------------------------------------------===\n\n";
	for(StringMapIterator<std::vector<VarMappingInfo> > i = mappingMap->begin();i!=mappingMap->end();++i){
		StringRef varName = i->getKey();
		//errs()<<"Info about variable: "<<varName<<"\n";
		for (std::vector<VarMappingInfo>::iterator j = (*mappingMap)[varName].begin(); j!= (*mappingMap)[varName].end(); ++j) {
			VarMappingInfo info = *j;
			//errs()<<"\t"<< info.nameIR << " -> " << info.context << " in: " << info.basicBlock<< " @ " << info.codeLine << "\n";
		}
	}
	//errs()<<"===-------------------------------------------------------------------------===\n\n";
	
}

void SafecodeVarMap::printReverseNameSCMap(StringMap< std::string > * reverseNameSCMap){
	//errs()<<"===-------------------------------------------------------------------------===\n";
	//errs() << "               ... Reverse Mapping Informations ...\n";
	//errs()<<"===-------------------------------------------------------------------------===\n\n";
	for(StringMapIterator<std::string> i = reverseNameSCMap->begin();i!=reverseNameSCMap->end();++i){
		StringRef varName = i->getKey();
		 //errs()<<"NameIR: "<<varName<<" -> "<< (&*i)->getValue() <<"\n";
	}
	 //errs()<<"===-------------------------------------------------------------------------===\n\n";
	
}

void SafecodeVarMap::printConstantsMap(StringMap< std::vector<std::string> > * constantsMap){
	 //errs()<<"===-------------------------------------------------------------------------===\n";
	 //errs() << "               ... Constans Mapping Informations ...\n";
	 //errs()<<"===-------------------------------------------------------------------------===\n\n";
	for(StringMapIterator<std::vector<std::string> > i = constantsMap->begin();i!=constantsMap->end();++i){
		StringRef varName = i->getKey();
		 //errs()<<"Constant: "<<varName<<"\n";
		for (std::vector<std::string>::iterator j = (*constantsMap)[varName].begin(); j!= (*constantsMap)[varName].end(); ++j) {
			 //errs()<<"\t"<<*j<<"\n";
		}
	}
	//errs()<<"===-------------------------------------------------------------------------===\n\n";
	
}

std::pair< std::string , ranges::Range> SafecodeVarMap::getRangePair(ACSLExpression *expr){
	if (ACSLBinaryExpression * binexp = llvm::dyn_cast<ACSLBinaryExpression>(expr)) {

		//case 1:"==";

		//case 3:"<";

		//case 4:">";

		//case 5:"<=";

		//case 6:">=";

		//case 7:"&&";

		//if it is a constant (ex. val==const):

		if(binexp->op==1){

			if (ACSLIdentifier * id = llvm::dyn_cast<ACSLIdentifier>(&(binexp->lhs))) {

				if (ACSLInteger * value = llvm::dyn_cast<ACSLInteger>(&(binexp->rhs))) {

					ranges::Range p(value->value, value->value);
					std::pair<std::string, ranges::Range > sp(id->name, p);
					return sp;

				}

			}

		}



		//if it is a range exp (ex. val>=const1 && val<=const2)

		if(binexp->op==7){

			if(ACSLBinaryExpression * binexp1 = llvm::dyn_cast<ACSLBinaryExpression>(&(binexp->lhs))){

				if(ACSLBinaryExpression * binexp2 = llvm::dyn_cast<ACSLBinaryExpression>(&(binexp->rhs))){

					//TODO: handle also expressions with the first operand constant (ex. 1<=val)

					if (ACSLIdentifier * id1 = llvm::dyn_cast<ACSLIdentifier>(&(binexp1->lhs))) {

						if (ACSLInteger * constant1 = llvm::dyn_cast<ACSLInteger>(&(binexp1->rhs))) {

							if (ACSLIdentifier * id2 = llvm::dyn_cast<ACSLIdentifier>(&(binexp2->lhs))) {

								if (ACSLInteger * constant2 = llvm::dyn_cast<ACSLInteger>(&(binexp2->rhs))) {



									int op1 = binexp1->op;

									int op2 = binexp2->op;



									std::string name1 = id1->name;

									int integer1 = constant1->value;



									std::string name2 = id2->name;

									int integer2 = constant2->value;



									//it should be about the same var

									if(name1==name2){

										//TODO: handle different combinations of <, >, ...

										if(op1==6 && op2==5){
											ranges::Range p(integer1, integer2);
											std::pair<std::string, ranges::Range > sp(name1, p);
											return sp;
										}

									}

								}

							}

						}

					}



				}

			}

		}

		//if it is a short range (ex. val==const1 || ... || val==constN)

		if (binexp->op==8) {

			if(ACSLBinaryExpression * binexp2 = llvm::dyn_cast<ACSLBinaryExpression>(&(binexp->rhs))){

				bool bottomFound = false;

				bool topFound = false;

				int top;

				int bottom;

				std::string varname;

				//take the top of the range

				if (binexp2->op==1) {

					if (ACSLInteger * constant2 = llvm::dyn_cast<ACSLInteger>(&(binexp2->rhs))) {

						if (ACSLIdentifier * id2 = llvm::dyn_cast<ACSLIdentifier>(&(binexp2->lhs))) {

							varname = id2->name;

							top = constant2->value;

							topFound=true;

						}

					}

				}

				//take the bottom of the range

				if(ACSLBinaryExpression * binexp1 = llvm::dyn_cast<ACSLBinaryExpression>(&(binexp->lhs))){

					ACSLBinaryExpression * tempExp = binexp1;

					while (!bottomFound) {

						if (tempExp->op==1) {

							if (ACSLInteger * constant1 = llvm::dyn_cast<ACSLInteger>(&(tempExp->rhs))) {

								if (ACSLIdentifier * id1 = llvm::dyn_cast<ACSLIdentifier>(&(tempExp->lhs))) {

									if(varname==id1->name){

										bottom = constant1->value;

										bottomFound = true;

									}

								}

							}

							break;

						}

						else if(tempExp->op==8){

							if(ACSLBinaryExpression * lhsTemp = llvm::dyn_cast<ACSLBinaryExpression>(&(tempExp->lhs))){

								tempExp = lhsTemp;

							}

						} else {

							break;

						}

					}

				}

				if (topFound && bottomFound) {
					ranges::Range p(bottom, top);
					std::pair<std::string, ranges::Range > sp(varname, p);
					return sp;
				}

			}

		}

	}
	return std::make_pair("#", ranges::Range(0,0));
}

}//end of anonymous namespace



char SafecodeVarMap::ID = 0;
static RegisterPass<SafecodeVarMap> X("safecodevarmap", "Safecode Acsl Variable Mapping Pass");
