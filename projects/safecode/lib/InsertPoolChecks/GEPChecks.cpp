//===- GEPChecks.cpp - Insert GEP run-time checks ------------------------- --//
// 
//                     The LLVM Compiler Infrastructure
//
// This file was developed by the LLVM research group and is distributed under
// the University of Illinois Open Source License. See LICENSE.TXT for details.
// 
//===----------------------------------------------------------------------===//
//
// This pass instruments GEPs with run-time checks to ensure safe array and
// structure indexing.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "safecode"

#include "llvm/ADT/Statistic.h"
#include "llvm/Support/CommandLine.h"
#include "safecode/GEPChecks.h"
#include "safecode/Utility.h"

#include "safecode/node.h"
#include "safecode/driver.h"

namespace llvm {

char InsertGEPChecks::ID = 0;

static RegisterPass<InsertGEPChecks>
X ("gepchecks", "Insert GEP run-time checks");

//
// Command Line Options
//

// Disable checks on pure structure indexing
cl::opt<bool> DisableStructChecks ("disable-structgepchecks", cl::Hidden,
                                   cl::init(false),
                                   cl::desc("Disable Struct GEP Checks"));

// Pass Statistics
namespace {
  STATISTIC (GEPChecks, "Bounds Checks Added");
  STATISTIC (SafeGEP,   "GEPs proven safe by SAFECode");
}


int InsertGEPChecks::getFunctionArgumentPosition(Value* op1, Function& F) {
	int place=0;
	for(Function::arg_iterator i = F.arg_begin(), e = F.arg_end();i!=e;++i) {
		if(op1==i) {
			return place;
		}
		else
			place++;
	}
	return -1;
}

//
// Method: visitGetElementPtrInst()
//
// Description:
//  This method checks to see if the specified GEP is safe.  If it cannot prove
//  it safe, it then adds a run-time check for it.
//
void
InsertGEPChecks::visitGetElementPtrInst (GetElementPtrInst & GEP) {
  //
  // Don't insert a check if GEP only indexes into a structure and the
  // user doesn't want to do structure index checking.
  //
  if (DisableStructChecks && indexesStructsOnly (&GEP)) {
    return;
  }

  //
  // Get the function in which the GEP instruction lives.
  //
  
  //begin niko
  if (MDNode *metaSize = GEP.getMetadata("acsl_malloc")) {
		int numElements = 0;
		MDString*  sizeMDString = dyn_cast<MDString>(metaSize->getOperand(0));
		std::string sizeString = sizeMDString->getString();
		std::stringstream(sizeString) >> numElements;
		if (MDNode *metaRange = GEP.getMetadata("acsl_safecode")) {
			Value* rangeAnnotationValue = metaRange->getOperand(0);
			MDString*  rangeAnnotationMDString = dyn_cast<MDString>(rangeAnnotationValue);
			StringRef rangeString = rangeAnnotationMDString->getString();
			if(inBounds(numElements, rangeString)) {
				CheckRemovedUsingInfo++;
				checksEliminated++;
				return;
			} else {
				OutOfBoundsInfo++;
			}
		} else {
			SizeInfo++;
		}
  } else if (MDNode *metaRange = GEP.getMetadata("acsl_safecode")) {
	IndexInfo++;
 } else {
	NoInfo++;
 }
  //end niko
  
  //rigel
  uint64_t numElements;
 	//GEP.print(errs());
	Value* op0 = GEP.getOperand(0); //NOTE: coremark has two-dimensional arrays, the GEP will be different
	//serrs() << "operand(0): " << *op0<<"\n"; //NOTE: do all this in terms of bytes, instead of number of elements
	if(isa<AllocaInst>(op0)) { //arrays
		Type* t = op0->getType();
		Type* tp = t->getPointerElementType();
		if(isa<ArrayType>(tp)) {
			ArrayType* typ = dyn_cast<ArrayType>(tp);
			numElements = typ->getArrayNumElements();
			if(doCheck(&GEP,numElements)) {
				checksEliminated++;
				return;
			}
		}
	}
	else if(isa<BitCastInst>(op0)){ //malloc, as far as we know
		llvm::LLVMContext &Context = GEP.getContext();
		unsigned ACSLMetadataKind = Context.getMDKindID("acsl_malloc");
		llvm::MDNode* mNode = GEP.getMetadata(ACSLMetadataKind);
		if(mNode) { //if the acsl metadata exists, it is because we put it there during the AcslVarMap pass
			 //errs() << *mNode<<"\n"; //this means that the instruction I is either a StoreInst or a LoadInst or a
			Value* annotation = mNode->getOperand(0);
			//annotation->print(errs());
			MDString*  annotation1 = dyn_cast<MDString>(annotation);
			StringRef ann = annotation1->getString();
			//errs() << ann;
			std::string anns = ann;
			std::stringstream(anns) >> numElements;
			if(doCheck(&GEP,numElements)) {
				checksEliminated++;
				return;
			}
		}
	}
	else  {
		Function* Fun = GEP.getParent()->getParent();
		int place=0;
		if((place = getFunctionArgumentPosition(op0, *Fun))!=-1){ //it has an acsl_malloc annotation
			llvm::LLVMContext &Context = GEP.getContext();
			unsigned ACSLMetadataKind = Context.getMDKindID("acsl_malloc");
			llvm::MDNode* mNode = GEP.getMetadata(ACSLMetadataKind);
			if(mNode) { //if the acsl metadata exists, it is because we put it there during the AcslVarMap pass
				 //errs() << *mNode<<"\n"; //this means that the instruction I is either a StoreInst or a LoadInst or a
				Value* annotation = mNode->getOperand(0);
				//annotation->print(errs());
				MDString*  annotation1 = dyn_cast<MDString>(annotation);
				StringRef ann = annotation1->getString();
				//errs() << ann;
				std::string anns = ann;
				std::stringstream(anns) >> numElements;
				if(doCheck(&GEP,numElements))
				  return;
			}
		}
	}
    
  //end rigel
  Value * PH = ConstantPointerNull::get (getVoidPtrType(GEP.getContext()));
  BasicBlock::iterator InsertPt = &GEP;
  ++InsertPt;
  Instruction * ResultPtr = castTo (&GEP,
                                    getVoidPtrType(GEP.getContext()),
                                    GEP.getName() + ".cast",
                                    InsertPt);

  //
  // Make this an actual cast instruction; it will make it easier to update
  // DSA.
  //
  Value * SrcPtr = castTo (GEP.getPointerOperand(),
                           getVoidPtrType(GEP.getContext()),
                           GEP.getName()+".cast",
                           InsertPt);

  //
  // Create the call to the run-time check.
  //
  std::vector<Value *> args(1, PH);
  args.push_back (SrcPtr);
  args.push_back (ResultPtr);
  CallInst * CI = CallInst::Create (PoolCheckArrayUI, args, "", InsertPt);

  //
  // Add debugging info metadata to the run-time check.
  //
  if (MDNode * MD = GEP.getMetadata ("dbg"))
    CI->setMetadata ("dbg", MD);

  //
  // Update the statistics.
  //
  ++GEPChecks;
  ++checksInserted;
  return;
}


bool InsertGEPChecks::doCheck(GetElementPtrInst* GEP, uint64_t numElements) {
	llvm::LLVMContext &Context = GEP->getContext();
	unsigned ACSLMetadataKind = Context.getMDKindID("acsl_safecode");
	llvm::MDNode* mNode = GEP->getMetadata(ACSLMetadataKind);
	if(mNode) { //if the acsl metadata exists, it is because we put it there during the AcslVarMap pass
		// () << *mNode<<"\n"; //this means that the instruction I is either a StoreInst or a LoadInst or a
		Value* annotation = mNode->getOperand(0);
		MDString*  annotation1 = dyn_cast<MDString>(annotation);
		StringRef ann = annotation1->getString();
		 //errs() << ann <<"\n";
		if(inBounds(numElements, ann)) {
			// errs() << "Eliminating Check for: "<<*inst<<"\n";
			 //errs() << "WITH metadata: "<<ann<<"\n";
			checksEliminated++;
			return true;
		}
		else 
			return false;
	}
	else { //if we have a constant index, there is no assertion, so we deal with things like array[0].
		if(GEP->getNumOperands() > 2) {
			Value* index = GEP->getOperand(2);
			if(ConstantInt* theIndex=dyn_cast<ConstantInt>(index)) {
				int64_t intIndex = theIndex->getSExtValue();
				//errs() << intIndex<<"\n";
				if(intIndex>=0 && intIndex<numElements) {
					checksEliminated++;
					return true;
					
				}
			}
		}
	}
	return false; //metadata does not exist, so we cannot say 
}					//anything and we must keep the runtime check

//StringRef ann: assert index >= lower && index < upper
//				 
bool InsertGEPChecks::inBounds(uint64_t numElements, StringRef annotation) {
	//errs() << annotation << "\n";
	ACSLStatements root;
    example::Driver driver(root);
    bool result = driver.parse_string(annotation, "input");
	if(!result)
		return false;
	
	//assert index >= lower && index < upper
	//                         stmt										driver.root.statements.push_back(stmt)
	//TASSERT(type)								ACSLBinaryExp
	//assert             "index    >=    lower",       7,         "index      <     upper"
	//
	//						     lhs				  7(op)		            rhs
	//					lhs      6       rhs					    lhs      3      rhs			
	//assert index ==5
	ACSLStatement* statement = driver.root.statements[0]; //assuming we have only one statement for now.
	ACSLBinaryExpression* expr = static_cast<ACSLBinaryExpression*>(&(statement->exp));
	ACSLInteger* upper;
	ACSLInteger* lower;
	switch(expr->op) {
		case 1: // index == 5
			upper = static_cast<ACSLInteger*>(&(expr->rhs));
			lower = static_cast<ACSLInteger*>(&(expr->rhs));
			break;
		case 7: //&&
			ACSLBinaryExpression* left1 = static_cast<ACSLBinaryExpression*>(&(expr->lhs));//.rhs;
			lower= static_cast<ACSLInteger*>(&(left1->rhs));
			ACSLBinaryExpression* right2 = static_cast<ACSLBinaryExpression*>(&(expr->rhs));//.rhs;
			upper= static_cast<ACSLInteger*>(&(right2->rhs));
			break;
	}
	//ACSLInteger upperbound = statement->exp.rhs.rhs;
	long long valLow = lower->value;
		//ACSLInteger upperbound = statement->exp.rhs.rhs;
	long long valUp = upper->value;
	//long long valUp = upperbound.value;
	if(valLow!=valUp)
		valUp--;
//	std::cout<<valLow << ":" <<valUp <<"\n";
	if(valLow>=0 && valUp<=numElements)
		return true;
	return false;
}

//
// Method: doInitialization()
//
// Description:
//  Perform module-level initialization before the pass is run.  For this
//  pass, we need to create a function prototype for the GEP check function.
//
// Inputs:
//  M - A reference to the LLVM module to modify.
//
// Return value:
//  true - This LLVM module has been modified.
//
bool
InsertGEPChecks::doInitialization (Module & M) {
  //
  // Create a function prototype for the function that performs incomplete
  // pointer arithmetic (GEP) checks.
  //
  Type * VoidPtrTy = getVoidPtrType (M.getContext());
  Constant * F = M.getOrInsertFunction ("boundscheckui",
                                        VoidPtrTy,
                                        VoidPtrTy,
                                        VoidPtrTy,
                                        VoidPtrTy,
                                        NULL);

  //
  // Mark the function as readonly; that will enable it to be hoisted out of
  // loops by the standard loop optimization passes.
  //
  (cast<Function>(F))->addFnAttr (Attributes::ReadOnly);
  return true;
}

bool
InsertGEPChecks::runOnFunction (Function & F) {

  //
  // Get pointers to required analysis passes.
  //Egm
  
  TD      = &getAnalysis<DataLayout>();
  abcPass = &getAnalysis<ArrayBoundsCheckLocal>();

  std::string fName = F.getName();
  std::string mName = F.getParent()->getModuleIdentifier();
  //errs() << "Module: "<< mName << " | GEPChecks::runOnFunction: " << fName << "\n";
  //
  // Get a pointer to the run-time check function.
  //
  PoolCheckArrayUI = F.getParent()->getFunction ("boundscheckui");

  //
  // Visit all of the instructions in the function.
  //
  visit (F);
  //rigel
 
  errs() << "Module: "<< mName << " | GEPChecks::runOnFunction: " << fName << "\n";
  std::cout<< "checksEliminated: " << checksEliminated << "\n";
  errs()<< "Checks inserted: " << checksInserted<<"\n";
  errs()<< "GEPChecks: " << GEPChecks<<"\n";
  checksEliminated=0;
  checksInserted = 0;
  //end rigel
  
  //begin niko
  errs()<<"Accesses with no metadata information: "<< NoInfo <<"\n";
  errs()<<"Accesses with only size metadata information: "<< SizeInfo <<"\n"; 
  errs()<<"Accesses with only index bounds information: "<< IndexInfo <<"\n"; 
  errs()<<"Accesses with index not inside size information: "<< OutOfBoundsInfo <<"\n"; 
  errs()<<"Checks removed using additional information: "<< CheckRemovedUsingInfo <<"\n"; 
  NoInfo = 0; 
  SizeInfo = 0; 
  IndexInfo = 0; 
  OutOfBoundsInfo = 0; 
  CheckRemovedUsingInfo = 0;
  //end niko
  return true;
}

}

