//===- InstrumentMemoryAccesses.cpp - Insert load/store checks ------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This pass instruments loads, stores, and other memory intrinsics with
// load/store checks by inserting the relevant __loadcheck and/or
// __storecheck calls before the them.
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "instrument-memory-accesses"

#include "CommonMemorySafetyPasses.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Constants.h"
#include "llvm/IntrinsicInst.h"
#include "llvm/IRBuilder.h"
#include "llvm/Pass.h"
#include "llvm/Support/InstVisitor.h"
#include "llvm/DataLayout.h"
#include "llvm/Transforms/Instrumentation.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Operator.h"
#include "safecode/node.h"
#include "safecode/driver.h"

using namespace llvm;

STATISTIC(LoadsInstrumented, "Loads instrumented");
STATISTIC(StoresInstrumented, "Stores instrumented");
STATISTIC(AtomicsInstrumented, "Atomic memory intrinsics instrumented");
STATISTIC(IntrinsicsInstrumented, "Block memory intrinsics instrumented");

namespace {
  class InstrumentMemoryAccesses : public FunctionPass,
                                   public InstVisitor<InstrumentMemoryAccesses> {
    const DataLayout *TD;

    IRBuilder<> *Builder;
	
	
    PointerType *VoidPtrTy;
    IntegerType *SizeTy;

    Function *LoadCheckFunction;
    Function *StoreCheckFunction;
	
    void instrument(Value *Pointer, Value *AccessSize, Function *Check,
                    Instruction &I);
	int getFunctionArgumentPosition(Value* op1, Function& F);
	bool inBounds(uint64_t numElements, StringRef ann);
	bool doCheck(Instruction* inst, uint64_t numElements);
  public:
    static char ID;
	int checksEliminated;
	int checksInserted;
	
	//begin niko
    int NoInfo; // Accesses with no metadata information
    int SizeInfo; // Accesses with only size metadata information
    int IndexInfo; // Accesses with only index bounds information
    int OutOfBoundsInfo; // Accesses with index not inside size information
    int CheckRemovedUsingInfo; // Accesses with index not inside size information
    //end niko
	
    InstrumentMemoryAccesses(): FunctionPass(ID), checksEliminated(0), checksInserted(0) { }
    virtual bool doInitialization(Module &M);
    virtual bool runOnFunction(Function &F);
	
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
      AU.addRequired<DataLayout>();
      AU.setPreservesCFG();
    }

    virtual const char *getPassName() const {
      return "InstrumentMemoryAccesses";
    }

    // Visitor methods
    void visitLoadInst(LoadInst &LI);
    void visitStoreInst(StoreInst &SI);
    void visitAtomicCmpXchgInst(AtomicCmpXchgInst &I);
    void visitAtomicRMWInst(AtomicRMWInst &I);
    void visitMemIntrinsic(MemIntrinsic &MI);
  };
} // end anon namespace

char InstrumentMemoryAccesses::ID = 0;



INITIALIZE_PASS(InstrumentMemoryAccesses, "instrument-memory-accesses",
                "Instrument memory accesses", false, false)

FunctionPass *llvm::createInstrumentMemoryAccessesPass() {
  return new InstrumentMemoryAccesses();
}

bool InstrumentMemoryAccesses::doInitialization(Module &M) {
  Type *VoidTy = Type::getVoidTy(M.getContext());
  VoidPtrTy = Type::getInt8PtrTy(M.getContext());
  SizeTy = IntegerType::getInt64Ty(M.getContext());

  // Create function prototypes
  M.getOrInsertFunction("__loadcheck", VoidTy, VoidPtrTy, SizeTy, NULL);
  M.getOrInsertFunction("__storecheck", VoidTy, VoidPtrTy, SizeTy, NULL);
  return true;
}

bool InstrumentMemoryAccesses::runOnFunction(Function &F) {
	//rigel
  std::string fName = F.getName();
  std::string mName = F.getParent()->getModuleIdentifier();
  errs() << "Module: "<< mName << " | InstrumentMemoryAccesses::runOnFunction: " << fName << "\n";
  // Check that the load and store check functions are declared.
  LoadCheckFunction = F.getParent()->getFunction("__loadcheck");
  assert(LoadCheckFunction && "__loadcheck function has disappeared!\n");

  StoreCheckFunction = F.getParent()->getFunction("__storecheck");
  assert(StoreCheckFunction && "__storecheck function has disappeared!\n");

  TD = &getAnalysis<DataLayout>();
  
 
  IRBuilder<> TheBuilder(F.getContext());
  Builder = &TheBuilder;

  // Visit all of the instructions in the function.
  visit(F);
  //rigel
  fName = F.getName();
  mName = F.getParent()->getModuleIdentifier();
  errs() << "Module: "<< mName << " | InstrumentMemoryAccesses::runOnFunction: " << fName << "\n";
  std::cout<< "        checksEliminated: " << checksEliminated << "\n";
  std::cout<< "        checks(loadstore)Inserted: " << checksInserted << "\n";
  errs()<< "Stores Instrumented: " << StoresInstrumented <<"\n";
  errs()<< "Loads Instrumented: " << LoadsInstrumented <<"\n";
  errs()<< "Atomics Instrumented: " << AtomicsInstrumented <<"\n";
  errs()<< "Intrinsics Instrumented: " << IntrinsicsInstrumented <<"\n";
  
  checksEliminated=0;
  checksInserted=0;
  //end rigel
  
  //begin niko
  errs()<<"Accesses with no metadata information: "<< NoInfo <<"\n";
  errs()<<"Accesses with only size metadata information: "<< SizeInfo <<"\n"; 
  errs()<<"Accesses with only index bounds information: "<< IndexInfo <<"\n"; 
  errs()<<"Accesses with index not inside size information: "<< OutOfBoundsInfo <<"\n"; 
  errs()<<"Checks removed using additional information: "<< CheckRemovedUsingInfo <<"\n"; 
  NoInfo = 0; 
  SizeInfo = 0; 
  IndexInfo = 0; 
  OutOfBoundsInfo = 0; 
  CheckRemovedUsingInfo = 0;
  //end niko
  return true;
}

void InstrumentMemoryAccesses::instrument(Value *Pointer, Value *AccessSize,
                                          Function *Check, Instruction &I) {
  Builder->SetInsertPoint(&I);
  Value *VoidPointer = Builder->CreatePointerCast(Pointer, VoidPtrTy);
  CallInst *CI = Builder->CreateCall2(Check, VoidPointer, AccessSize);

  // Copy debug information if it is present.
  if (MDNode *MD = I.getMetadata("dbg"))
    CI->setMetadata("dbg", MD);
}

bool InstrumentMemoryAccesses::doCheck(Instruction* inst, uint64_t numElements) {
	llvm::LLVMContext &Context = inst->getContext();
	unsigned ACSLMetadataKind = Context.getMDKindID("acsl_safecode");
	llvm::MDNode* mNode = inst->getMetadata(ACSLMetadataKind);
	if(mNode) { //if the acsl metadata exists, it is because we put it there during the AcslVarMap pass
		Value* annotation = mNode->getOperand(0);
		MDString*  annotation1 = dyn_cast<MDString>(annotation);
		StringRef ann = annotation1->getString();
		 //errs() << ann <<"\n";
		if(inBounds(numElements, ann)) {
			 //errs() << "Eliminating Check for: "<<*inst<<"\n";
			 //errs() << "WITH metadata: "<<ann<<"\n";
			checksEliminated++;
			return true;
		}
		else 
			return false;
	}
	else { //if we have a constant index, there is no assertion, so we deal with things like array[0].
		if(GetElementPtrInst *GEP = dyn_cast<GetElementPtrInst>(inst)) {
			if(GEP->getNumOperands() > 2) {
				Value* index = GEP->getOperand(2);
				if(ConstantInt* theIndex=dyn_cast<ConstantInt>(index)) {
					uint64_t intIndex = theIndex->getZExtValue();
					//errs() << intIndex<<"\n";
					if(intIndex>=0 && intIndex<numElements) {
						checksEliminated++;
						return true;
						
					}
				}
			}
				
		}
	}
	return false; //metadata does not exist, so we cannot say 
}					//anything and we must keep the runtime check

void InstrumentMemoryAccesses::visitLoadInst(LoadInst &LI) {
  //rigel
  uint64_t numElements;
  //errs() << LI<<"\n";
  Value* V = LI.getPointerOperand(); 		//StoreInst and LoadInst. The GetElementPtrInst will be dealt with in the
  //errs() << *V<<"\n";				 			//InsertGEPChecks pass
  if(GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(V)) { //probably unnecessary to do this check, since it was done in AcslVarMap	 
	  
	  //begin niko
	  if (MDNode *metaSize = gep->getMetadata("acsl_malloc")) {
			int numElements = 0;
			MDString*  sizeMDString = dyn_cast<MDString>(metaSize->getOperand(0));
			std::string sizeString = sizeMDString->getString();
			std::stringstream(sizeString) >> numElements;
			if (MDNode *metaRange = gep->getMetadata("acsl_safecode")) {
				Value* rangeAnnotationValue = metaRange->getOperand(0);
				MDString*  rangeAnnotationMDString = dyn_cast<MDString>(rangeAnnotationValue);
				StringRef rangeString = rangeAnnotationMDString->getString();
				if(inBounds(numElements, rangeString)) {
					CheckRemovedUsingInfo++;
					checksEliminated++;
					return;
				} else {
					OutOfBoundsInfo++;
				}
			} else {
				SizeInfo++;
			}
	  } else if (MDNode *metaRange = gep->getMetadata("acsl_safecode")) {
		IndexInfo++;
	 } else {
		NoInfo++;
	 }
	  //end niko
	
	
	
	Value* op0 = gep->getOperand(0); //NOTE: coremark has two-dimensional arrays, the GEP will be different
	//errs() << "operand(0): " << *op0<<"\n"; //NOTE: do all this in terms of bytes, instead of number of elements
	if(isa<AllocaInst>(op0)) { //arrays
		Type* t = op0->getType();
		Type* tp = t->getPointerElementType();
		if(isa<ArrayType>(tp)) {
			  ArrayType* typ = dyn_cast<ArrayType>(tp);
			  numElements = typ->getArrayNumElements();
			   //errs() << "array num elements: " << numElements << "\n";
			  if(doCheck(gep,numElements))
				  return;
		}
	}
	else if(isa<BitCastInst>(op0)){ //malloc, as far as we know
		llvm::LLVMContext &Context = gep->getContext();
		unsigned ACSLMetadataKind = Context.getMDKindID("acsl_malloc");
		llvm::MDNode* mNode = gep->getMetadata(ACSLMetadataKind);
		if(mNode) { //if the acsl metadata exists, it is because we put it there during the AcslVarMap pass
			 //errs() << *mNode<<"\n"; //this means that the instruction I is either a StoreInst or a LoadInst or a
			Value* annotation = mNode->getOperand(0);
			//annotation->print(errs());
			MDString*  annotation1 = dyn_cast<MDString>(annotation);
			StringRef ann = annotation1->getString();
			//errs() << ann;
			std::string anns = ann;
			std::stringstream(anns) >> numElements;
			if(doCheck(gep,numElements))
			  return;
		}
	}
	else  {
		Function* Fun = LI.getParent()->getParent();
		int place=0;
		if((place = getFunctionArgumentPosition(op0, *Fun))!=-1){ //it has an acsl_malloc annotation
			llvm::LLVMContext &Context = gep->getContext();
			unsigned ACSLMetadataKind = Context.getMDKindID("acsl_malloc");
			llvm::MDNode* mNode = gep->getMetadata(ACSLMetadataKind);
			if(mNode) { //if the acsl metadata exists, it is because we put it there during the AcslVarMap pass
				 //errs() << *mNode<<"\n"; //this means that the instruction I is either a StoreInst or a LoadInst or a
				Value* annotation = mNode->getOperand(0);
				//annotation->print(errs());
				MDString*  annotation1 = dyn_cast<MDString>(annotation);
				StringRef ann = annotation1->getString();
				//errs() << ann;
				std::string anns = ann;
				std::stringstream(anns) >> numElements;
				if(doCheck(gep,numElements))
				  return;
			}
		}
		
	 }
  }
  else if(ConcreteOperator<Operator,Instruction::GetElementPtr> *PI =
	dyn_cast<ConcreteOperator<Operator,Instruction::GetElementPtr> >(V)) {
	 //errs() << "PI: " << *PI<<"\n";
	Value* theGlobal = PI->getOperand(0);//@global_array = common global [10000 x i32] zeroinitializer, align 16
	 //errs() <<"operand(0): "<<*theGlobal<<"\n";
	if(GlobalVariable* gv = dyn_cast<GlobalVariable>(theGlobal)) {
		//errs()<<"gv is global";
		Type* typ = gv->getType();//[10000 x i32]*
		//errs() <<"Type: "<<*typ<<"\n";
		Type* pointedType=typ->getPointerElementType();//[10000 x i32]
		//errs() <<"Pointed Type: "<< *pointedType <<"\n";
		if(isa<ArrayType>(pointedType)){ //OK we are accessing an array (which is global)
			unsigned int numOperands = PI->getNumOperands();
			//errs()<<"numOperands: " << numOperands<<"\n";
			Value* indexValue = PI->getOperand(numOperands-1);
			if(ConstantInt* index = dyn_cast<ConstantInt>(indexValue)) {
				//errs() << "index is constant: "<< *index <<"\n";
				numElements = pointedType->getArrayNumElements();
				//errs() << "array num elements: " << numElements << "\n";
				if(doCheck(&LI,numElements))
					return;
			}
		}
	}
  }
  // Instrument a load instruction with a load check.
  Value *AccessSize = ConstantInt::get(SizeTy,
                                       TD->getTypeStoreSize(LI.getType()));
  instrument(LI.getPointerOperand(), AccessSize, LoadCheckFunction, LI);
  ++LoadsInstrumented;
  ++checksInserted;
}

int InstrumentMemoryAccesses::getFunctionArgumentPosition(Value* op1, Function& F) {
	int place=0;
	for(Function::arg_iterator i = F.arg_begin(), e = F.arg_end();i!=e;++i) {
		//errs()<<*i;
		if(op1==i) {
			return place;
		}
		else
			place++;
	}
	return -1;
}

void InstrumentMemoryAccesses::visitStoreInst(StoreInst &SI) {
  // Instrument a store instruction with a store check.
  //rigel
  uint64_t numElements;
   //errs() << SI<<"\n";
  Value* V = SI.getPointerOperand(); 		//StoreInst and LoadInst. The GetElementPtrInst will be dealt with in the
   //errs() << *V<<"\n";				 			//InsertGEPChecks pass
  if(GetElementPtrInst* gep = dyn_cast<GetElementPtrInst>(V)) { //probably unnecessary to do this check, since it was done in AcslVarMap	 

	  //begin niko
	  if (MDNode *metaSize = gep->getMetadata("acsl_malloc")) {
			int numElements = 0;
			MDString*  sizeMDString = dyn_cast<MDString>(metaSize->getOperand(0));
			std::string sizeString = sizeMDString->getString();
			std::stringstream(sizeString) >> numElements;
			if (MDNode *metaRange = gep->getMetadata("acsl_safecode")) {
				Value* rangeAnnotationValue = metaRange->getOperand(0);
				MDString*  rangeAnnotationMDString = dyn_cast<MDString>(rangeAnnotationValue);
				StringRef rangeString = rangeAnnotationMDString->getString();
				if(inBounds(numElements, rangeString)) {
					CheckRemovedUsingInfo++;
					checksEliminated++;
					return;
				} else {
					OutOfBoundsInfo++;
				}
			} else {
				SizeInfo++;
			}
	  } else if (MDNode *metaRange = gep->getMetadata("acsl_safecode")) {
		IndexInfo++;
	 } else {
		NoInfo++;
	 }
	 //end niko
	
	
	Value* op0 = gep->getOperand(0); //NOTE: coremark has two-dimensional arrays, the GEP will be different
	//errs() << "operand(0): " << *op0<<"\n"; //NOTE: do all this in terms of bytes, instead of number of elements
	if(isa<AllocaInst>(op0)) { //arrays
		Type* t = op0->getType();
		Type* tp = t->getPointerElementType();
		if(isa<ArrayType>(tp)) {
			  ArrayType* typ = dyn_cast<ArrayType>(tp);
			  numElements = typ->getArrayNumElements();
			   //errs() << "array num elements: " << numElements << "\n";
			  if(doCheck(gep,numElements))
				  return;
		}
	}
	else if(isa<BitCastInst>(op0)){ //malloc, as far as we know
		llvm::LLVMContext &Context = gep->getContext();
		unsigned ACSLMetadataKind = Context.getMDKindID("acsl_malloc");
		llvm::MDNode* mNode = gep->getMetadata(ACSLMetadataKind);
		if(mNode) { //if the acsl metadata exists, it is because we put it there during the AcslVarMap pass
			 //errs() << *mNode<<"\n"; //this means that the instruction I is either a StoreInst or a LoadInst or a
			Value* annotation = mNode->getOperand(0);
			//annotation->print(errs());
			MDString*  annotation1 = dyn_cast<MDString>(annotation);
			StringRef ann = annotation1->getString();
			//errs() << ann;
			std::string anns = ann;
			std::stringstream(anns) >> numElements;
			if(doCheck(gep,numElements))
			  return;
		}
	}
	else  {
		Function* Fun = SI.getParent()->getParent();
		int place=0;
		if((place = getFunctionArgumentPosition(op0, *Fun))!=-1){ //it has an acsl_malloc annotation
			llvm::LLVMContext &Context = gep->getContext();
			unsigned ACSLMetadataKind = Context.getMDKindID("acsl_malloc");
			llvm::MDNode* mNode = gep->getMetadata(ACSLMetadataKind);
			if(mNode) { //if the acsl metadata exists, it is because we put it there during the AcslVarMap pass
				 //errs() << *mNode<<"\n"; //this means that the instruction I is either a StoreInst or a LoadInst or a
				Value* annotation = mNode->getOperand(0);
				//annotation->print(errs());
				MDString*  annotation1 = dyn_cast<MDString>(annotation);
				StringRef ann = annotation1->getString();
				//errs() << ann;
				std::string anns = ann;
				std::stringstream(anns) >> numElements;
				if(doCheck(gep,numElements))
				  return;
			}
		}
		
	}
  }
  else if(ConcreteOperator<Operator,Instruction::GetElementPtr> *PI =
	dyn_cast<ConcreteOperator<Operator,Instruction::GetElementPtr> >(V)) { //for global variables
	 //errs() << "PI: " << *PI<<"\n";
	Value* theGlobal = PI->getOperand(0);//@global_array = common global [10000 x i32] zeroinitializer, align 16
	 //errs() <<"operand(0): "<<*theGlobal<<"\n";
	if(GlobalVariable* gv = dyn_cast<GlobalVariable>(theGlobal)) {
		 //errs()<<"gv is global";
		Type* typ = gv->getType();//[10000 x i32]*
		 //errs() <<"Type: "<<*typ<<"\n";
		Type* pointedType=typ->getPointerElementType();//[10000 x i32]
		 //errs() <<"Pointed Type: "<< *pointedType <<"\n";
		if(isa<ArrayType>(pointedType)){ //OK we are accessing an array (which is global)
			unsigned int numOperands = PI->getNumOperands();
			 //errs()<<"numOperands: " << numOperands<<"\n";
			Value* indexValue = PI->getOperand(numOperands-1);
			if(ConstantInt* index = dyn_cast<ConstantInt>(indexValue)) {
				 //errs() << "index is constant: "<< *index <<"\n";
				numElements = pointedType->getArrayNumElements();
				 //errs() << "array num elements: " << numElements << "\n";
				if(doCheck(&SI,numElements))
					return;
			}
		}
	}
  }
			
  //end rigel
  uint64_t Bytes = TD->getTypeStoreSize(SI.getValueOperand()->getType());
  Value *AccessSize = ConstantInt::get(SizeTy, Bytes);
  instrument(SI.getPointerOperand(), AccessSize, StoreCheckFunction, SI);
  ++StoresInstrumented;
  ++checksInserted;
}

//StringRef ann: assert index >= lower && index < upper
//		 
bool InstrumentMemoryAccesses::inBounds(uint64_t numElements, StringRef annotation) {
	ACSLStatements root;
    example::Driver driver(root);
    bool result = driver.parse_string(annotation, "input");
	
	//assert index >= lower && index < upper
	//                         stmt										driver.root.statements.push_back(stmt)
	//TASSERT(type)								ACSLBinaryExp
	//assert             "index    >=    lower",       7,         "index      <     upper"
	//
	//						     lhs				  7(op)		            rhs
	//					lhs      6       rhs					    lhs      3      rhs			
	//assert index ==5
	ACSLStatement* statement = driver.root.statements[0]; //assuming we have only one statement for now.
	ACSLBinaryExpression* expr = static_cast<ACSLBinaryExpression*>(&(statement->exp));
	ACSLInteger* upper;
	ACSLInteger* lower;
	switch(expr->op) {
		case 1: // index == 5
			upper = static_cast<ACSLInteger*>(&(expr->rhs));
			lower = static_cast<ACSLInteger*>(&(expr->rhs));
			break;
		case 7: //&&
			ACSLBinaryExpression* left1 = static_cast<ACSLBinaryExpression*>(&(expr->lhs));//.rhs;
			
			lower= static_cast<ACSLInteger*>(&(left1->rhs));
			ACSLBinaryExpression* right2 = static_cast<ACSLBinaryExpression*>(&(expr->rhs));//.rhs;
			upper= static_cast<ACSLInteger*>(&(right2->rhs));
			break;
	}
	//ACSLInteger upperbound = statement->exp.rhs.rhs;
	long long valLow = lower->value;
		//ACSLInteger upperbound = statement->exp.rhs.rhs;
	long long valUp = upper->value;
	//long long valUp = upperbound.value;
	//if(valLow!=valUp)
	//	valUp--;
//	std::cout<<valLow << ":" <<valUp <<"\n";
	if(valLow >= 0 && valUp<numElements) {
		
		return true;
	
	}
	return false;
}

void InstrumentMemoryAccesses::visitAtomicRMWInst(AtomicRMWInst &I) {
  // Instrument an AtomicRMW instruction with a store check.
  Value *AccessSize = ConstantInt::get(SizeTy,
                                       TD->getTypeStoreSize(I.getType()));
  instrument(I.getPointerOperand(), AccessSize, StoreCheckFunction, I);
  ++AtomicsInstrumented;
}

void InstrumentMemoryAccesses::visitAtomicCmpXchgInst(AtomicCmpXchgInst &I) {
  // Instrument an AtomicCmpXchg instruction with a store check.
  Value *AccessSize = ConstantInt::get(SizeTy,
                                       TD->getTypeStoreSize(I.getType()));
  instrument(I.getPointerOperand(), AccessSize, StoreCheckFunction, I);
  ++AtomicsInstrumented;
}

void InstrumentMemoryAccesses::visitMemIntrinsic(MemIntrinsic &MI) {
  // Instrument llvm.mem[set|cpy|move].* calls with load/store checks.
  Builder->SetInsertPoint(&MI);
  Value *AccessSize = Builder->CreateIntCast(MI.getLength(), SizeTy,
                                             /*isSigned=*/false);

  // memcpy and memmove have a source memory area but memset doesn't
  if (MemTransferInst *MTI = dyn_cast<MemTransferInst>(&MI))
    instrument(MTI->getSource(), AccessSize, LoadCheckFunction, MI);
  instrument(MI.getDest(), AccessSize, StoreCheckFunction, MI);
  ++IntrinsicsInstrumented;
}
