//===- GEPChecks.h - Insert run-time checks for GEPs -------------------------//
// 
//                     The LLVM Compiler Infrastructure
//
// This file was developed by the LLVM research group and is distributed under
// the University of Illinois Open Source License. See LICENSE.TXT for details.
// 
//===----------------------------------------------------------------------===//
//
// This file implements several passes that insert run-time checks to enforce
// SAFECode's memory safety guarantees as well as several other passes that
// help to optimize the instrumentation.
//
//===----------------------------------------------------------------------===//

#ifndef _SAFECODE_GEP_CHECKS_H_
#define _SAFECODE_GEP_CHECKS_H_

#include "llvm/Analysis/Dominators.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Instructions.h"
#include "llvm/Pass.h"
#include "llvm/Support/InstVisitor.h"
#include "llvm/DataLayout.h"

#include "safecode/ArrayBoundsCheck.h"

namespace llvm {

//
// Pass: InsertGEPChecks
//
// Description:
//  This pass inserts checks on GEP instructions.
//
struct InsertGEPChecks : public FunctionPass, InstVisitor<InsertGEPChecks> {
	public:
    static char ID;
	int checksEliminated;
	int checksInserted;
	//begin niko
    int NoInfo; // Accesses with no metadata information
    int SizeInfo; // Accesses with only size metadata information
    int IndexInfo; // Accesses with only index bounds information
    int OutOfBoundsInfo; // Accesses with index not inside size information
    int CheckRemovedUsingInfo; // Checks removed using additional information
    //end niko
  
    InsertGEPChecks () : FunctionPass (ID), checksEliminated(0) {
		//begin niko
		NoInfo = 0; 
		SizeInfo = 0; 
		IndexInfo = 0; 
		OutOfBoundsInfo = 0; 
		CheckRemovedUsingInfo = 0;
		//end niko
	}
    const char *getPassName() const { return "Insert GEP Checks"; }
    virtual bool  doInitialization (Module & M);
    virtual bool runOnFunction(Function &F);
	int getFunctionArgumentPosition(Value* op1, Function& F);
	bool doCheck(GetElementPtrInst* inst, uint64_t numElements);
	bool inBounds(uint64_t numElements, StringRef annotation);
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
      // Required passes
      AU.addRequired<DataLayout>();
      AU.addRequired<ArrayBoundsCheckLocal>();

      // Preserved passes
      AU.setPreservesCFG();
    };

    // Visitor methods
    void visitGetElementPtrInst (GetElementPtrInst & GEP);

  protected:
    // Pointers to required passes
    DataLayout * TD;
    ArrayBoundsCheckLocal * abcPass;

    // Pointer to GEP run-time check function
    Function * PoolCheckArrayUI;
};

}
#endif
